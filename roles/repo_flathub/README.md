Ansible Role: `repo_flathub`
============================

Install Flatpak and configure Flathub repository.


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

