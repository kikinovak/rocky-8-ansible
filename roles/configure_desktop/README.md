Ansible Role: `configure_desktop`
=================================

Modify KDE's default configuration:

- Disable Baloo file indexing

- Lock screen after 60 minutes

- Enable wobbly windows and magic lamp

- Default to Breeze Light theme

- MIME associations for video files


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

