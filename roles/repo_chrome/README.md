Ansible Role: `repo_chrome`
===========================

Configure Chrome package repository with a priority of 10.


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

