Ansible Role: `fonts_microsoft`
===============================

Install Microsoft TrueType fonts:

- Microsoft Core Fonts

- Microsoft Tahoma

- Microsoft Vista Fonts

- Lucida Console


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

