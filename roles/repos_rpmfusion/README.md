Ansible Role: `repos_rpmfusion`
===============================

Configure RPM Fusion package repositories with a priority of 20:

  - RPM Fusion Free

  - RPM Fusion Free Tainted

  - RPM Fusion Nonfree


Requirements
------------

EPEL (`repos_epel` role) needs to be enabled before.


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

