Ansible Role: `apps_utilities`
==============================

Install Applications > Utilities:

- Ark

- K3B

- KDE Connect

- KeePassXC


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

