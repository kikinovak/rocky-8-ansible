Ansible Role: `repos_epel`
==========================

Configure EPEL package repositories with a priority of 10:

  - EPEL

  - EPEL Modular


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

