Ansible Role: `repos_official`
==============================

Configure official Rocky Linux package repositories with a priority of 1:

  - BaseOS

  - AppStream

  - Extras

  - Powertools


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

