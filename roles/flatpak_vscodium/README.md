Ansible Role: `flatpak_vscodium`
================================

Install VSCodium Flatpak.


Requirements
------------

Flathub (`repo_flathub`) must be configured.


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

