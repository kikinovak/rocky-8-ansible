Ansible Role: `fonts_eurostile`
===============================

Install Eurostile TrueType fonts.


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

