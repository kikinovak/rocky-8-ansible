Ansible Role: `flatpak_upgrade`
===============================

Update all Flatpak applications.


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

