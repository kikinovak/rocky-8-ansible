Ansible Role: `desktop_extras`
==============================

Install KDE desktop enhancements like wallpapers and Conky.


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

