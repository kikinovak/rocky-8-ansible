Ansible Role: `check_system`
============================

Check if we're running Rocky Linux 8.x.


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

