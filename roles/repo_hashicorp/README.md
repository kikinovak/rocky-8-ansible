Ansible Role: `repo_hashicorp`
==============================

Configure Hashicorp package repository with a priority of 10.


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

