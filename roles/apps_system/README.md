Ansible Role: `apps_system`
===========================

Install Applications > System:

- AnyDesk

- Neofetch

- Pinentry

- Virtual Machine Manager


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

