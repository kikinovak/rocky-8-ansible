Ansible Role: `apps_graphics`
=============================

Install Applications > Graphics:

- Digikam

- GIMP + plugins

- Inkscape

- SANE scanner drivers

- Simple Scan


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

