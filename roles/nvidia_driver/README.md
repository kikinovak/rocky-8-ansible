Ansible Role: `nvidia_driver`
=============================

Install proprietary NVidia video driver.


Requirements
------------

ELRepo repository (`repos_elrepo`) must be enabled.


Role Variables
--------------

- `nvidia_driver`: video driver version

This can take one of the following values:

- `None` (default)

- `550xx`

- `470xx`

- `390xx`

Use the `nvidia-detect` tool to find out the correct driver version for your
card.


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

