Ansible Role: `flatpak_teams`
=============================

Install Microsoft Teams Flatpak.


Requirements
------------

Flathub (`repo_flathub`) must be configured.


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

