Ansible Role: `configure_ssh`
=============================

Make sure SSH doesn't inherit system locales like `LANG` from client systems.


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

