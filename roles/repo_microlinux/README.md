Ansible Role: `repo_microlinux`
===============================

Configure Microlinux package repository with a priority of 10.


Requirements
------------

EPEL (`repos_epel` role) needs to be enabled before.


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

