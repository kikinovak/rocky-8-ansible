Ansible Role: `install_xorg`
============================

Install X.org along with some basic X11 fonts and the NVidia detection tool.


Requirements
------------

Enabled package repositories:

  - Official (`repos_official`)

  - ELRepo (`repos_epel`)


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

