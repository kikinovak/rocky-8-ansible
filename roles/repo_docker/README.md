Ansible Role: `repo_docker`
===========================

Configure Docker package repository with a priority of 10.


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

