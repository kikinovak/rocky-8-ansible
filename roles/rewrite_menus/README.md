Ansible Role: `rewrite_menus`
=============================

Install enhanced desktop menu entries.

Available languages:

 - English

 - French

 - German


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

