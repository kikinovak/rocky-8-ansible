Ansible Role: `configure_locale`
================================

Set parameters for:

- System locale

- Console keyboard layout

- X11 keyboard layout


Role Variables
--------------

- `locale`: defaults to `fr_FR.utf8`. Other possible values are `de_AT.utf8`,
  `en_US.utf8`, etc.

- `console_keymap`: defaults to `ch-fr`. Other possible values are `fr`, `de`,
  `us`, etc.

- `x11_keymap`: defaults to `ch pc105 fr`. Other possible values are `fr`,
  `de`, etc.


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

