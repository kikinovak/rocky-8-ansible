Ansible Role: `apps_langpacks`
==============================

Install language packs:

- English

- French

- German


License
-------

BSD


Author Information
------------------

Niki Kovacs <info@microlinux.fr>

